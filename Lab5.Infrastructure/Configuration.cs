﻿using System;
using PK.Container;
using Glowna;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            Container kontener = new Container();
            //kontener.Register<Glowna.IGlowny>(new Glowny());
            kontener.Register<Glowna.Glowny>(new Glowny());
            kontener.Register<
            return kontener;
        }
    }
}
